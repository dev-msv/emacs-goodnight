;;; goodnight.el --- Switch between themes automatically at specified time -*- lexical-binding:t -*-

;; Copyright (C) 2023, 2024  Moisés Simón Vázquez

;; Author: Moisés Simón Vázquez <devmsv@posteo.net>
;; URL: https://gitlab.com/dev-msv/emacs-goodnight
;; Mailing-List: none yet
;; Version: 0.1
;; Package-Requires: ((emacs "29.1"))
;; Keywords: convenience, theme

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:
;;
;; This package automatically switch between two themes (usually light and dark)
;; at user specified HOUR or local sunrise/sunset.  It is also possible to manually switch themes at any moment.



;;; Code:


(require 'xdg)
(require 'solar)

(defgroup goodnight ()
  "User options for Goodnight."
  :group 'faces
  :prefix "goodnight-"
  :tag "Goodnight")

(defcustom goodnight-light 'modus-operandi
  "Light theme variant."
  :group 'goodnight
  :type '(symbol))

(defcustom goodnight-dark 'modus-vivendi
  "Dark theme variant."
  :group 'goodnight
  :type '(symbol))

(defcustom goodnight-gtk-light "Adwaita-light"
  "Light theme for GTK."
  :group 'goodnight
  :type '(string))

(defcustom goodnight-gtk-dark "Adwaita-dark"
  "Dark theme for GTK."
  :group 'goodnight
  :type '(string))

(defcustom goodnight-kde-light "BreezeLight"
  "Light theme for KDE."
  :group 'goodnight
  :type '(string))

(defcustom goodnight-kde-dark "BreezeDark"
  "Dark theme for KDE."
  :group 'goodnight
  :type '(string))

(defun goodnight--get-sunrise-timestamp ()
  "Timestamp at which sunrise occurs."
  (let ((calendar-time-display-form '(24-hours ":" minutes)))
    (apply #'solar-time-string
	   (car (solar-sunrise-sunset (calendar-current-date))))))

(defcustom goodnight-light-at-timestamp (goodnight--get-sunrise-timestamp)
  "Timestamp for `runt-at-time'."
  :group 'goodnight
  :type '(string))

(defun goodnight--get-sunset-timestamp ()
  "Timestamp at which sunset occurs."
  (let ((calendar-time-display-form '(24-hours ":" minutes)))
    (apply #'solar-time-string
	   (cadr (solar-sunrise-sunset (calendar-current-date))))))

(defcustom goodnight-dark-at-timestamp (goodnight--get-sunset-timestamp)
  "Timestamp for `runt-at-time'."
  :group 'goodnight
  :type '(string))

(defcustom goodnight-xsettingsd-file (catch 'loop
				       (let ((xsettingsd-files
					      (list (expand-file-name "xsettingsd/xsettingsd.conf" (xdg-config-home))
						    (expand-file-name "~/.xsettingsd"))))
					 (dolist (file xsettingsd-files)
					   (when (file-readable-p
						  file)
					     (throw 'loop file)))))
  "Xsettings configuration file."
  :group 'goodnight
  :type '(string))

(defvar goodnight-old-themes '()
  "List to hold loaded theme before `goodnight-start' is executed.")

;; TODO: desktop could change if running emacs-daemon
(defconst goodnight-desktop
  (let ((desktop (getenv "XDG_SESSION_DESKTOP")))
    (if desktop
	desktop
      "exwm"))
  "Value of `XDG_SESSION_DESKTOP' or \"exwm\" if `XDG_SESSION_DESKTOP' is empty.")

(defvar timer-goodnight-dark nil
  "Value of `goodnight' timer to set dark theme.")

(defvar timer-goodnight-light nil
  "Value of `goodnight' timer to set light theme.")

(defvar timer-goodnight-refresh nil
  "Value of `goodnight' timer to refresh timers.")

(defvar goodnight--system-colorscheme "light"
  "ColorScheme from system (either KDE, GNOME or xsettingsd).")

(defvar goodnight--emacs-gsettings (when (require 'gsettings nil t)
				     (gsettings-available?)))

(defvar goodnight--emacs-exwm-xsettings (when (require 'exwm-xsettings nil t)
					  (fboundp #'exwm-xsettings--pick-theme)))

(defvar goodnight--gsettings (executable-find "gsettings"))

(defvar goodnight--dconf (executable-find "dconf"))



(defun goodnight-get-system-colorscheme ()
  "Get ColorScheme from system (either KDE, GNOME or xsettingsd)."
  (let ((goodnight--system-colorscheme nil))
    (pcase goodnight-desktop
      ("kde"
       (if (equal (string-trim-right
		   (shell-command-to-string "kreadconfig5 --group General --key ColorScheme"))
		  goodnight-kde-dark)
	   (setq goodnight--system-colorscheme "dark")
	 (setq goodnight--system-colorscheme "light")))
      ((or "gnome" "gnome-xorg")
       (if (string= (string-trim (if goodnight--emacs-gsettings
				     (gsettings-get "org.gnome.desktop.interface" "color-scheme")
				   (if goodnight--gsettings
				       (string-trim-right (shell-command-to-string "gsettings get org.gnome.desktop.interface color-scheme"))
				     (if goodnight--dconf
					 (string-trim-right (shell-command-to-string "dconf read /org/gnome/desktop/interface/color-scheme"))
				       (error "goodnight: There is no program to get gnome color-scheme."))))
				 "'" "'")
		    "default")
	   (setq goodnight--system-colorscheme "light")
	 (setq goodnight--system-colorscheme "dark")))
      ("exwm"
       (if (and (not goodnight-xsettingsd-file)
		goodnight--emacs-exwm-xsettings)
	   (setq goodnight--system-colorscheme
		 (if (string= (face-background 'default)
			      "unspecified-bg")
		     "light"
		   (exwm-xsettings--pick-theme '("light" . "dark"))))
	 (if goodnight-xsettingsd-file
	     (with-temp-buffer
	       (insert-file-contents goodnight-xsettingsd-file nil)
	       (if (search-forward goodnight-gtk-dark nil t)
		   (setq goodnight--system-colorscheme "dark")
		 (setq goodnight--system-colorscheme "light")))
	   (error "goodnight.el: No system-theme handler")))))
    goodnight--system-colorscheme))

(defun goodnight-kde-change-variant (variant)
  "Change KDE ColorScheme-colorscheme according to VARIANT."
  (pcase variant
    ("light"
     (call-process-shell-command (concat "plasma-apply-colorscheme " goodnight-kde-light)))
    ("dark"
     (call-process-shell-command (concat "plasma-apply-colorscheme " goodnight-kde-dark))))
  (goodnight-get-system-colorscheme))

(defun goodnight-gnome-change-variant (variant)
  "Change GNOME ColorScheme-colorscheme according to VARIANT."
  (pcase variant
    ("light"
     (if goodnight--emacs-gsettings
	 (gsettings-set-from-gvariant-string "org.gnome.desktop.interface" "color-scheme" "default")
       (if goodnight--gsettings
	   (call-process-shell-command "gsettings set \"org.gnome.desktop.interface\" \"color-scheme\" \"'default'\"")
	 (if goodnight--dconf
	     (call-process-shell-command "dconf write /org/gnome/desktop/interface/color-scheme \"'default'\"")
	   (error "goodnight.el: Unable to set 'gnome-color-scheme'")))))
    ("dark"
     (if goodnight--emacs-gsettings
	 (gsettings-set-from-gvariant-string "org.gnome.desktop.interface" "color-scheme" "prefer-dark")
       (if goodnight--gsettings
	   (call-process-shell-command "gsettings set \"org.gnome.desktop.interface\" \"color-scheme\" \"'prefer-dark'\"")
	 (if goodnight--dconf
	     (call-process-shell-command "dconf write /org/gnome/desktop/interface/color-scheme \"'prefer-dark'\"")
	   (error "goodnight.el: Unable to set 'gnome-color-scheme'"))))))
  (goodnight-get-system-colorscheme))

(defun goodnight-exwm-change-variant (variant)
  "Change theme VARIANT in file `goodnight-xsettingsd-file' and refresh daemon."
  (let ((xsettingsd-pid (goodnight-get-xsettingsd-pid)))
    (if (and (not goodnight-xsettingsd-file)
	     goodnight--emacs-exwm-xsettings)
	(pcase variant
	  ("dark"
	   (load-theme goodnight-dark))
	  ("light"
	   (load-theme goodnight-light))))
    (if goodnight-xsettingsd-file
	(with-temp-file goodnight-xsettingsd-file
	  (insert-file-contents goodnight-xsettingsd-file nil)
	  (pcase variant
	    ("dark"
	     ;; we use 'when' here because if 'search-forward' is nil
	     ;; replace-match will error and stop execution
	     ;; TODO: handle error replacing
	     ;; TODO: make sure we in the line starting with Net/ThemeName
	     (when (search-forward goodnight-gtk-light nil t)
	       (replace-match goodnight-gtk-dark nil nil)))
	    ("light"
	     (when (search-forward goodnight-gtk-dark nil t)
	       (replace-match goodnight-gtk-light nil nil))))))
    (goodnight-gnome-change-variant variant)
    (if xsettingsd-pid
	(signal-process xsettingsd-pid 'SIGHUP))))

(defun goodnight-load-emacs-variant ()
  "Adjust Emacs theme to system-colorscheme."
  (let* ((system-colorscheme (goodnight-get-system-colorscheme))
	 (new-theme (if (string-equal system-colorscheme "light")
			'goodnight-light
		      'goodnight-dark)))
    (dolist (theme custom-enabled-themes)
      (disable-theme theme))
    (load-theme (symbol-value new-theme))))

(defun goodnight-change-wm-theme (variant)
  "Switch system theme-VARIANT.  If VARIANT is `'toggle' toggle the theme."
  (when (equal variant 'toggle)
    (let ((current-theme (symbol-name (car custom-enabled-themes)))
	  (dark-theme (symbol-name goodnight-dark))
	  (light-theme (symbol-name goodnight-light)))
      (cond
       ((string-equal current-theme dark-theme)
	(goodnight-change-wm-theme "light"))
       ((string-equal current-theme light-theme)
	(goodnight-change-wm-theme "dark")))))
  (when (equal variant 'auto)
    (let ((current-hour (string-to-number (format-time-string "%-H" (current-time))))
	  (dark-hour (string-to-number (car (split-string goodnight-dark-at-timestamp ":"))))
	  (light-hour (string-to-number (car (split-string goodnight-light-at-timestamp ":")))))
      (if (and (>= current-hour light-hour)
	       (< current-hour dark-hour))
	  (goodnight-change-wm-theme "light")
	(goodnight-change-wm-theme "dark"))))
  (when (or (string-equal variant "light")
	    (string-equal variant "dark"))
    (pcase goodnight-desktop
      ("plasma" (goodnight-kde-change-variant variant))
      ("exwm" (goodnight-exwm-change-variant variant))
      ((or "gnome" "gnome-xorg") (goodnight-gnome-change-variant variant)))))

(defun goodnight-toggle-theme (&optional variant)
  "Change theme-variant to VARIANT.

If called without prefix toggle VARIANT
If called with one prefix switch to `goodnight-light'
If called with negative or two prefix switch to `goodnight-dark'

If called from lisp: switch to `goodnight-light' if ARG is
\"light\" switch to `goodnight-dark' if ARG is \"dark\" switch to
theme-variant depending on `goodnight-{dark,light}-hour' if ARG
is `\'auto'."
  (interactive "p")
  (pcase variant
    ((or 1 'toggle)
     (goodnight-change-wm-theme 'toggle))
    (4
     (goodnight-change-wm-theme "light"))
    ((or -1 16)
     (goodnight-change-wm-theme "dark"))
    ((or "dark" "light" 'auto)
     (goodnight-change-wm-theme variant)))

  (if (and goodnight-xsettingsd-file
	   (not (string= goodnight-desktop "exwm")))
      (goodnight-load-emacs-variant)))

;; This is mostly for clarity on the `list-timers'
(defun goodnight-set-theme-dark ()
  "Set theme to dark."
  (goodnight-toggle-theme "dark"))

(defun goodnight-set-theme-light ()
  "Set theme to light."
  (goodnight-toggle-theme "light"))

;; `goodnight-refresh-timer' must be treated specially to prevent loops
(defun goodnight-setup-refresh-timer ()
  "Set `goodnight-refresh-timer', only once."
  (unless timer-goodnight-refresh
    (setq timer-goodnight-refresh (run-at-time "00:01" (* 24 3600) 'goodnight-refresh-timers))))

(defun goodnight-cancel-refresh-timer ()
  "Stop `goodnight-refresh-timer'."
  (when timer-goodnight-refresh
    (cancel-timer timer-goodnight-refresh)
    (setq timer-goodnight-refresh nil)))

(defun goodnight-setup-timers ()
  "Set timers at `goodnight-dark-at-timestamp' and `goodnight-light-at-timestamp'."
  (when timer-goodnight-dark
    (cancel-timer timer-goodnight-dark))
  (setq timer-goodnight-dark (run-at-time goodnight-dark-at-timestamp  (* 24 3600) 'goodnight-set-theme-dark))
  (when timer-goodnight-light
    (cancel-timer timer-goodnight-light))
  (setq timer-goodnight-light (run-at-time goodnight-light-at-timestamp (* 24 3600) 'goodnight-set-theme-light))
  (goodnight-setup-refresh-timer))

(defun goodnight-cancel-timers ()
  "Stop goodnight timers."
  (when timer-goodnight-dark
    (cancel-timer timer-goodnight-dark)
    (setq timer-goodnight-dark nil))
  (when timer-goodnight-light
    (cancel-timer timer-goodnight-light)
    (setq timer-goodnight-light nil)))

(defun goodnight-refresh-timers ()
  "Refresh the timers according to sunrise and sunset for today date.

This command can also be run to refresh sunrise/sunset after
`calendar-longitude' and `calendar-latitude' has been changed."
  (interactive)
  (setq goodnight-dark-at-timestamp (goodnight--get-sunset-timestamp)
	goodnight-light-at-timestamp (goodnight--get-sunrise-timestamp))
  (goodnight-cancel-timers)
  (goodnight-setup-timers))

(defun goodnight-get-xsettingsd-pid ()
  "Get xsettingd pid.
Return nil if there is no xsettingd running."
  (let (pid)
    (dolist (proc (list-system-processes) pid)
      (when (string-equal "xsettingsd"
			  (cdr (assoc 'comm (process-attributes proc))))
	(setq pid proc)))))

(defun goodnight-start-xsettingsd ()
  "Start xsettingsd daemon."
  (let ((proc-goodnight-xsettingsd
	 (start-process "xsettingsd"
			" *xsettingsd*"
			"xsettingsd" "-c" goodnight-xsettingsd-file)))
    (set-process-query-on-exit-flag proc-goodnight-xsettingsd nil)))

(defun goodnight-stop-xsettingsd ()
  "Stop xsettingsd daemon."
  (signal-process (goodnight-get-xsettingsd-pid) 'SIGKILL))

(defun goodnight--enable-mode ()
  "Setup goodnight timers and auto-load corresponding theme."
  (when (and goodnight-xsettingsd-file
	     (not goodnight--emacs-exwm-xsettings))
    (goodnight-start-xsettingsd))
  (goodnight-setup-timers)
  (dolist (theme custom-enabled-themes)
    (add-to-list 'goodnight-old-themes theme))
  (goodnight-toggle-theme 'auto))

(defun goodnight--disable-mode ()
  "Stop goodnight timers and load old theme."
  (when (and goodnight-xsettingsd-file
	     (not goodnight--emacs-exwm-xsettings))
    (goodnight-stop-xsettingsd))
  (goodnight-cancel-timers)
  (goodnight-cancel-refresh-timer)
  (dolist (theme goodnight-old-themes)
    (load-theme theme)))

;;;###autoload
(define-minor-mode goodnight-mode
  "Setup `goodnight-mode'."
  :global t
  (if goodnight-mode
      (goodnight--enable-mode)
    (goodnight--disable-mode)))

(provide 'goodnight)
;;; goodnight.el ends here
